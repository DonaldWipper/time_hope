
import html
import requests
import collections
import json
import urllib.request, urllib.error
from geopy.geocoders import Nominatim

#https://api.instagram.com/v1/tags/{tag-name}?access_token=ACCESS-TOKEN
#https://www.instagram.com/developer/endpoints/tags/
#https://api.instagram.com/v1/locations/{location-id}?access_token=ACCESS-TOKEN
#https://stackoverflow.com/questions/40542335/how-to-get-all-images-of-hashtag-in-instagram-without-api
#https://developers.google.com/maps/documentation/javascript/markers?hl=ru

#1724579101014762310_5500519126
#"location":{"id":"222482974"

hashtags = ["бивеньтур", "tbt", "timehope", "timehop"]
#BgJdGZuFBm8
CLIENT_ID = "f77fdb68a34548c4a223675c6cc4204c"
CLIENT_SECRET = "999e8961e0f84349aac2201c8d2e3dc3"
redirect_uri = "https://gitlab.com/DonaldWipper/time_hope"
code = "2b3817506820436a8b54dba5ea3e21c9"
access_token = "2936761451.f77fdb6.916ee2c2e7394ce0a406cf0b2ce6ddb3"
scope = "public_content"

subscription_key = "f3ca474524d045ecb62236061b51cd2"
#f3ca474524d045ecb62236061b51cd2

short_cuts = []
tag_req = "https://api.instagram.com/v1/tags/"  

tagы = "https://www.instagram.com/explore/tags/" #timehop/
emotion_recognition_url = "https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize"

MAX_TAG_ID =  10000 
MIN_TAG_ID = 0
count = 100

users = []
parse_result = {
           "user_name": "",
           "full_name":"",
           "text":"",
           "likes":0,
           "facebook":"",
           "vk":"",
           "location_name":"",
           "location_id":"",
           "latitude": "",
           "longitude": "",
           "country":"",
           "biography":"",
           "connected_fb_page":None
          }

parse_result2 = {
           "user_name": "",
           "full_name":"",
           "text":"",
           "likes":0,
           "facebook":"",
           "vk":"",
           "location_name":"",
           "location_id":"",
           "latitude": "",
           "longitude": "",
           "country":"",
           "biography":"",
           "connected_fb_page":None
          }


#https://elfsight.com/blog/2016/05/how-to-get-instagram-access-token/

#https://api.instagram.com/oauth/authorize/?client_id=CLIENT-ID&redirect_uri=REDIRECT-URI&response_type=code

#https://api.instagram.com/oauth/authorize/?client_id=f77fdb68a34548c4a223675c6cc4204c&redirect_uri=https://gitlab.com/DonaldWipper/time_hope&response_type=code
#7779d70362af4c14aa211d6cfe04a6db
#http://api.instagram.com/oembed?url=http://instagram.com/p/Y7GF-5vftL/



def get_inst_auth_app():
    url = ""
    url += "https://api.instagram.com/oauth/authorize/"
    url += "?client_id=" + CLIENT_ID
    url += "&redirect_uri=" + redirect_uri
    url += "&response_type=" + "code" 
    url += "&scope=" + scope
    print(url) 
    print(requests.get(url)) 

def get_inst_outh_token():
    url = ""
    url += "https://api.instagram.com/oauth/access_token/"
    '''
    url += "&client_secret=" + CLIENT_SECRET
    url += "&redirect_uri=" + redirect_uri
    url += "&response_type=" + code 
    print(url) 
    '''
    data = requests.post(url, data = {"client_id": CLIENT_ID, "client_secret": CLIENT_SECRET, "grant_type":"authorization_code", 
                                      "redirect_uri":redirect_uri,
                                      "code" :code}).text
    print(data)

def get_inst_info_by_tag():
    url = tag_req + hashtags[0] + "?access_token=" +access_token
    print(url)
    print(requests.get(url).json()) 

def get_records_by_tag():
    url = tag_req + hashtags[0] + "/media/recent" +  "?access_token=" +access_token
    url += "&count=" + str(count)   
    url += "&min_tag_id=" + str(MIN_TAG_ID)
    url += "&max_tag_id=" + str(MAX_TAG_ID)
    print(url)
    print(requests.get(url).text)  
 
def get_emotions_by_photo(image_path) :
    image_data = open(image_path, "rb").read()
    headers  = {'Ocp-Apim-Subscription-Key': subscription_key, "Content-Type": "application/octet-stream" }
    response = requests.post(emotion_recognition_url, headers=headers, data=image_data)
    response.raise_for_status()
    analysis = response.json()
    analysis

def save_result(output, r): 
    with open(output, 'a') as fp:
        json.dump(r, fp)

def set_loc_coords(id): 
    global parse_result 
    url = "https://api.instagram.com/v1/locations/" + id + "?access_token=" + access_token
    res = requests.get(url).json()
    parse_result["latitude"] = res["data"]["latitude"]
    parse_result["longitude"] = res["data"]["longitude"]
    try:
        parse_result["country"] = get_country_by_coords(parse_result["latitude"], parse_result["longitude"])
    except:
        parse_result["country"] = None

def set_bio(name):
    url = "https://www.instagram.com/" + name + "/?__a=1"
    res = requests.get(url).json()
    parse_result["biography"] = res["user"]["biography"]
    parse_result["connected_fb_page"] = res["user"]["connected_fb_page"]


def get_info(shortcode): 
    global parse_result 
    url = "https://www.instagram.com/p/" + shortcode + "/?__a=1"
    res = requests.get(url).json()
    res = res["graphql"]["shortcode_media"]
    parse_result["user_name"] = res["owner"]["username"]
    set_bio(res["owner"]["username"])
    parse_result["full_name"] = res["owner"]["full_name"]
    #print(res["location"])
    if res["location"] != None:
        parse_result["location_name"] = res["location"]["name"]
        parse_result["location_id"] = res["location"]["id"]
        #set_loc_coords(res["location"]["id"])


def get_country_by_coords(lat, lng):
    '''
    url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + str(lat) + "," + str(lng) + "&sensor=false"
    #print(url)
    res = requests.get(url).json()
    lngth = len(res["results"])
    print(res["results"][lngth-1]['address_components'][0]['short_name'])
    return res["results"][lngth-1]['address_components'][0]['short_name']    
    '''
    geolocator = Nominatim()
    location = geolocator.reverse(str(lat) + "," +  str(lng))
    print(location.address)





def save_images_by_tag2(hashtag, count):
    url = "https://www.instagram.com/explore/tags/" + hashtag + "/?__a=1" 
    res = requests.get(url).json()
    print(url) 
    global parse_result  
    count_ = 0
    res = res["graphql"]["hashtag"]["edge_hashtag_to_media"]
    while  True:
        next = res["page_info"]["has_next_page"]
        edges = res["edges"]
        for edge in edges:
            parse_result = parse_result2
            url = edge["node"]["display_url"]
            try:
               text = edge["node"]["edge_media_to_caption"]["edges"][0]["node"]["text"]
            except:
               text = ""
            likes =  edge["node"]["edge_liked_by"]["count"]
            #parse_result["text"] = text
            parse_result["likes"] = likes
            shortcode = edge["node"]["shortcode"]
            #get_info(shortcode)
            #path = "images/" + hashtag + str(count_) + ".jpg"
            #urllib.request.urlretrieve(url, path)
            if  parse_result["country"] == "RU":
                path = "images/" + hashtag + str(count_) + ".jpg"
                urllib.request.urlretrieve(url, path)   
            save_result("res.txt", parse_result)
            count_ += 1
            print(str(count_))
            if count_ > count:
                break;   
        if count_ > count:
            break;      
        end = res["page_info"]["end_cursor"]
        if next  == False:
            break; 
        url = "https://www.instagram.com/explore/tags/" + hashtag + "/?__a=1" + "&max_id=" + end
        res = requests.get(url).json()
        res = res["graphql"]["hashtag"]["edge_hashtag_to_media"]
  

#https://www.instagram.com/p/BgJe_JIjnrC/?__a=1


def main():
    #get_inst_auth_app()
    #get_inst_outh_token()
    #get_inst_info_by_tag() 
    #get_records_by_tag()  
    #save_images_by_tag2(hashtags[3], 1000000) 
    print(get_country_by_coords(10, 20))
    #print(get_info("BdbPi2NhO6z")) 
main()
    
	
